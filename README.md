# Yoti iOS Hiring Test

**Document Version 1.0**

This repository contains the coding exercise for Yoti's iOS Role. 

## Context

Try to solve the problem of displaying Country and Capital. This application is made with Xcode 12.4 and test on iOS 14.4

------

## What i have changed

I have modified the CountryNetworkService.swift class so now we require a generic type which is inherited by Codeable type. 
This makes this class Generic so it is scable if requried for further models.

The call is made by `CountryNetworkService<CountriesResponse>.perform`

I have also added `NetworkError` Enum which inherits from Error and helps with NetworkResponse.

I have Embedded ViewController in NavigationController so there is Title and made it large Display title

Tableview is added to ViewController and the celltype is Subtitle.


