//
// Copyright © 2021 Yoti Ltd. All rights reserved.
//

import UIKit

class CountriesAndCapitalsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var countriesAndCapitals = [CountriesResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set up delegate and datasource
        tableView.delegate = self
        tableView.dataSource = self
        
        // Navigation Title and diaplay large
        self.title = "Countries And Capital"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        loadCountriesAndCapital()
    }
    
    func loadCountriesAndCapital() {
        if let url = URL(string: Constants.APIURL.COUNTRIES_URL) {
            // call the network call passing the CountriesResponse where the JSON will be stored.
            // switch on the result, if success store the reponse into local Array else currently printing err, (should show the alert)
            CountryNetworkService<CountriesResponse>.perform(for: url) { (result) in
                switch result {
                case .success(let response):
                    self.countriesAndCapitals = response
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                case .failure(let err):
                    print(err)
                }
            }
        }
    }
}

extension CountriesAndCapitalsViewController: UITableViewDelegate {
    // delegate methods
}

extension CountriesAndCapitalsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return count of countriesAndCapital array
        return countriesAndCapitals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Dequeue the tableview cell with cell Identifier
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCells.CountriesCell.CellIdentifier) {
            cell.textLabel?.text = countriesAndCapitals[indexPath.row].name
            
            // need to check if capital is empty, if empty provide default text.
            if countriesAndCapitals[indexPath.row].capital.isEmpty {
                cell.detailTextLabel?.text = "No Capital Returned"
            } else {
                cell.detailTextLabel?.text = countriesAndCapitals[indexPath.row].capital
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
}

