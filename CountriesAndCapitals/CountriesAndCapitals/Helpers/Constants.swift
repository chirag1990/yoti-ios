//
// Copyright © 2021 Yoti Ltd. All rights reserved.
//


import Foundation

public enum Constants {
    
    public enum APIURL {
        public static let COUNTRIES_URL = "https://restcountries.eu/rest/v2/all"
    }
    
    public enum TableViewCells {
        public enum CountriesCell {
            public static let CellIdentifier = "countryCell"
        }
    }
}
