//
// Copyright © 2021 Yoti Ltd. All rights reserved.
//


import Foundation
// Created a Model to hold data for response.
struct CountriesResponse: Codable {
    var name: String
    var capital: String
}
