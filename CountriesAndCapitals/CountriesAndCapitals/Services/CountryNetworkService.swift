//
// Copyright © 2021 Yoti Ltd. All rights reserved.
//


import Foundation

// NetworkError enum which has cases to handle network respones.
enum NetworkError: Error {
    case invalidResponse
    case invalidData
    case error(err: String)
    case decodingError(err: String)
}

// Made a class which takes in generic codable class.
final class CountryNetworkService<T: Codable> {
    static func perform(for url: URL, completion: @escaping (Result<[T], NetworkError>) -> Void) {
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            // check if the error is not nil
            guard error == nil else {
                // completion is failure
                completion(.failure(.error(err: error?.localizedDescription ?? "Unknown Error")))
                return
            }
            
            // check http response is 200 if not pass the completion as failure
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
                completion(.failure(.invalidResponse))
                return
            }
            
            // unwrap the optional data else pass completein as failure
            guard let data = data else {
                completion(.failure(.invalidData))
                return
            }
            
            // if we reach till here, can try and decode the JSON using the generic T.
            // if sucessful in decoding, pass the JSON through completion. else pass the error through completion.
            do {
                let json = try JSONDecoder().decode([T].self, from: data)
                completion(.success(json))
            } catch let err {
                print(err)
                completion(.failure(.decodingError(err: err.localizedDescription)))
            }
            
        }.resume()
        
    }
}
